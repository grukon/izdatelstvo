<?php

namespace app\components;

use yii\base\Widget;
use app\models\Prise;

class PriseWidget extends Widget{
    
    public $vid;
  
    public function init(){
        parent::init();
    }
    
    public function run(){
        
       
        
        $post = Prise::find()->where(['activ' => 'yes'])->orderBy('id ASC')->all();
        
         return $this->render('prise', compact('post'));
       
        
    }
   

   
   
   
   
}