<?php

namespace app\components;

use yii\base\Widget;
use app\models\Settings;

class SettingsWidget extends Widget{
    
    public $vid;
  
    public function init(){
        parent::init();
    }
    
    public function run(){
        $post = Settings::find()->where(['id' => 1])->one();
        
        
        $vid = $this->vid;
        
        
        return $this->render('settings', compact('post','vid'));
       
    }
   

   
   
   
   
}