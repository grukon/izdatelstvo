<?php

namespace app\components;

use yii\base\Widget;
use app\models\States;

class StatesWidget extends Widget{
    
  
    public function init(){
        parent::init();
    }
    
    public function run(){
        $post = States::find()->where(['activ' => 'yes'])->orderBy('id DESC')->all();
        
         return $this->render('states', compact('post'));
       
        
    }
   

   
   
   
   
}