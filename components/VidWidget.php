<?php

namespace app\components;

use yii\base\Widget;
use app\models\Vid;

class VidWidget extends Widget{
    
    public $vid;
  
    public function init(){
        parent::init();
    }
    
    public function run(){
        
        $vid = $this->vid;
        
        $post = Vid::find()->where(['activ' => 'yes'])->orderBy('id ASC')->all();
        
         return $this->render('vid', compact('post','vid'));
       
        
    }
   

   
   
   
   
}