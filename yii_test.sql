-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июл 31 2019 г., 00:12
-- Версия сервера: 10.3.16-MariaDB
-- Версия PHP: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `yii_test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `secTitle` varchar(255) NOT NULL,
  `pages` varchar(255) NOT NULL,
  `dateReceived` date DEFAULT NULL,
  `artType` varchar(255) NOT NULL,
  `authors_id` int(11) NOT NULL,
  `artTitle` varchar(255) NOT NULL,
  `abstract` mediumtext NOT NULL,
  `text` mediumtext NOT NULL,
  `magazine_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `secTitle`, `pages`, `dateReceived`, `artType`, `authors_id`, `artTitle`, `abstract`, `text`, `magazine_id`, `lang_id`) VALUES
(30, 'От главного редактора', '9-18', NULL, 'REV', 1022545, 'ЭПОХА БАРОККО - МАГИСТРАЛИ ХУДОЖЕСТВЕННОГО ТВОРЧЕСТВА (Очерк второй)', 'В рубрике От главного редактора продолжается публикация большой серии художественно-исторических эссе, в которых последовательно рассматриваются крупнейшие эпохи. Эти обзоры призваны наметить панораму эволюции человечества в призме реалий всех видов искусства с выходом на онтологические обобщения. В предыдущих эссе были рассмотрены Древний мир, Античность, Средневековье и Возрождение.', 'Солирующая здесь флейта (так же, как и клавесин, она была излюбленным тембром музыки рококо) буквально искрится живой радостью жизни, игрой «солнечных зайчиков».', 28, 0),
(31, 'Отечественная история', '19-23', '2019-01-19', 'RAR', 1022546, 'ДЕНЕЖНАЯ ИНФЛЯЦИЯ И ХЛЕБНЫЕ ПОСТАВКИ В СОБЫТИЯХ ОКТЯБРЯ 1917 ГОДА В РОССИИ (НА МАТЕРИАЛАХ ПЕТЕРБУРГСКОЙ И МОСКОВСКОЙ ГУБЕРНИЙ)', 'В статье анализируется взаимосвязь инфляционных процессов в период с 1913 г. по октябрь 1917 г. в Российской империи и обрушения поставок хлеба в Санкт-Петербурге и Москве и их губерниях накануне и в ходе начавшихся революционных событий. С помощью корреляционного анализа была выявлена обратная сильная зависимость между этими двумя процессами, где инфляция была первичной причиной, а недостаток хлеба - результирующей. Впервые из данных официальных статистических источников было установлено, что в Петрограде недопоставки хлеба были вызваны обесцениванием денежной массы на 96%, а в Москве - на 93%.', 'Можно предположить, что подобный подход мог бы быть интересен в изучении других пар факторов, а установление количественных мер их взаимосвязи помогло бы по-иному взглянуть на совокупные причины революции 1917 года в России.', 28, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `authors`
--

CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `inner_id` int(21) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `initials` varchar(255) NOT NULL,
  `orgName` varchar(255) NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `authors`
--

INSERT INTO `authors` (`id`, `inner_id`, `surname`, `initials`, `orgName`, `lang_id`) VALUES
(1022545, 706352, 'Демченко', 'Александр Иванович', 'Центр комплексных художественных исследований Саратовской государственной консерватории имени Л. В. Собинова', 0),
(1022546, 582789, 'Акашева', 'Анна Анатольевна', 'Национальный исследовательский Нижегородский государственный университет имени Н. И. Лобачевского', 0),
(1022547, 1022469, 'Соколов', 'Юрий Вячеславович', 'Национальный исследовательский Нижегородский государственный университет имени Н. И. Лобачевского', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `codes`
--

CREATE TABLE `codes` (
  `id` int(11) NOT NULL,
  `name` varchar(48) NOT NULL,
  `value` varchar(255) NOT NULL,
  `article_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `codes`
--

INSERT INTO `codes` (`id`, `name`, `value`, `article_id`) VALUES
(4, 'doi', '10.30853/manuscript.2019.5.1', 30),
(5, 'doi', '10.30853/manuscript.2019.5.2', 31),
(6, 'udk', '94(47)“1917”+339.13.053.1', 31);

-- --------------------------------------------------------

--
-- Структура таблицы `errors_log`
--

CREATE TABLE `errors_log` (
  `id` int(11) NOT NULL,
  `type` varchar(48) NOT NULL,
  `field` varchar(48) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `fields`
--

CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `fields_values`
--

CREATE TABLE `fields_values` (
  `id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `keywords`
--

CREATE TABLE `keywords` (
  `id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `value_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `keywords`
--

INSERT INTO `keywords` (`id`, `article_id`, `value_id`) VALUES
(49, 31, 17),
(50, 31, 18),
(51, 31, 19),
(52, 31, 20),
(53, 31, 21),
(54, 31, 22),
(55, 31, 23),
(56, 31, 24),
(57, 31, 25),
(58, 31, 26),
(59, 31, 27),
(60, 31, 28),
(61, 31, 29),
(62, 31, 30),
(63, 31, 31),
(64, 31, 32);

-- --------------------------------------------------------

--
-- Структура таблицы `keywords_values`
--

CREATE TABLE `keywords_values` (
  `id` int(11) NOT NULL,
  `caption` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `keywords_values`
--

INSERT INTO `keywords_values` (`id`, `caption`) VALUES
(17, 'Великая российская революция'),
(18, 'Первая мировая война'),
(19, 'денежное обращение'),
(20, 'инфляция'),
(21, 'хлебный вопрос'),
(22, 'продовольственный фактор'),
(23, 'взаимосвязь факторов'),
(24, 'корреляционный анализ'),
(25, 'The Great Russian Revolution'),
(26, 'The First World War'),
(27, 'money circulation'),
(28, 'inflation'),
(29, 'bread problem'),
(30, 'food factor'),
(31, 'interaction of factors'),
(32, 'correlation analysis');

-- --------------------------------------------------------

--
-- Структура таблицы `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `languages`
--

INSERT INTO `languages` (`id`, `caption`, `name`) VALUES
(1, 'Русский', 'RUS'),
(2, 'Английский', 'ENG');

-- --------------------------------------------------------

--
-- Структура таблицы `magazines`
--

CREATE TABLE `magazines` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `bookType` varchar(48) NOT NULL,
  `dateUni` varchar(255) NOT NULL,
  `publ` varchar(255) NOT NULL,
  `publid` int(11) NOT NULL,
  `placePubl` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `author_id` int(11) NOT NULL,
  `pages` varchar(255) NOT NULL,
  `langPubl` varchar(255) NOT NULL,
  `titleid` varchar(255) NOT NULL,
  `issn` varchar(255) NOT NULL,
  `eissn` varchar(255) NOT NULL,
  `volume` int(15) NOT NULL,
  `number` int(15) NOT NULL,
  `altNumber` int(15) NOT NULL,
  `part` varchar(255) NOT NULL,
  `issTitle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `magazines`
--

INSERT INTO `magazines` (`id`, `title`, `bookType`, `dateUni`, `publ`, `publid`, `placePubl`, `author_id`, `pages`, `langPubl`, `titleid`, `issn`, `eissn`, `volume`, `number`, `altNumber`, `part`, `issTitle`) VALUES
(28, 'Манускрипт', '', '2019', '', 0, '', 0, '1-218', '', '66794', '2618-9690', '', 12, 5, 0, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `opercards`
--

CREATE TABLE `opercards` (
  `id` int(11) NOT NULL,
  `operator` varchar(255) NOT NULL,
  `pid` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `cntArticle` int(11) NOT NULL,
  `cntNode` varchar(255) NOT NULL,
  `cs` int(11) NOT NULL,
  `magazine_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `opercards`
--

INSERT INTO `opercards` (`id`, `operator`, `pid`, `date`, `cntArticle`, `cntNode`, `cs`, `magazine_id`) VALUES
(4, 'Articulus_2516', 451637, '2019-04-10 10:45:06', 45, '', 0, 28);

-- --------------------------------------------------------

--
-- Структура таблицы `references_t`
--

CREATE TABLE `references_t` (
  `id` int(11) NOT NULL,
  `caption` text NOT NULL,
  `article_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `references_t`
--

INSERT INTO `references_t` (`id`, `caption`, `article_id`) VALUES
(761, 'Беляев С. Г. Государственный бюджет России в годы Первой мировой войны // На пути к революционным потрясениям: экономика, государственный строй и социальные отношения в России второй половины XIX - начала XX века: сб. ст. Международной научной конференции (г. Санкт-Петербург, 24-25 мая 1999 г.). СПб., 1999. С. 299-310.', 31),
(762, 'Бородкин Л. И. Уровень жизни населения городов России в годы Первой мировой войны: обвал 1917 г. // Память о прошлом - 2017: материалы и доклады VI Историко-архивного форума, посвященного 100-летию революции 1917 г. в России / сост. О. Н. Солдатова, Г. С. Пашковская. М., 2017. С. 23-30.', 31),
(763, 'Гаффаров И. З. Финансово-бюджетная система Казанской губернии в революционные месяцы 1917-1918 гг. // Наука. Мысль. 2017. № 1-2. С. 9-12.', 31),
(764, 'История России XX - начала XXI века: учеб. пособие / под ред. Л. В. Милова. М.: Эксмо, 2010. 960 с.', 31),
(765, 'Карпачёв М. Д. Кризис продовольственного снабжения в годы Первой мировой войны (по материалам Воронежской губернии) // Российская история. 2011. № 3. С. 66-81.', 31),
(766, 'Касимов А. С. Продовольственный кризис в земледельческих губерниях Центральной России накануне Февральской революции // Известия Пензенского государственного педагогического университета им. В. Г. Белинского. Серия «Гуманитарные науки». 2007. № 4 (8). С. 91-94.', 31),
(767, 'Китанина Т. М. Война, хлеб и революция. Продовольственный вопрос в России. 1914 - октябрь 1917 г. Л.: Наука, 1985. 388 с.', 31),
(768, 'Кондратьев Н. Д. Рынок хлебов и его регулирование во время войны и революции. М.: Наука, 1991. 487 с.', 31),
(769, 'Лейберов И. П., Рудаченко С. Д. Революция и хлеб. М.: Мысль, 1990. 222 с.', 31),
(770, 'Мамаев А. В. Российская революция 1917 г. и муниципальные финансы // Вестник Владимирского государственного университета. Серия «Социальные и гуманитарные науки». 2017. № 3 (15). С. 23-30.', 31),
(771, 'Миронов Б. Н. Благосостояние населения и революции в имперской России: XVIII - начало XX века. М.: Весь мир, 2012. 844 с.', 31),
(772, 'Нефедов С. А. Продовольственный кризис в Петрограде накануне февральской революции // Quaestio Rossica. 2017. Т. 5. № 3. С. 635-655.', 31),
(773, 'Оськин М. В. Продовольственный вопрос в начале 1917 года: в кольце кризисов и переворотов // 1917 год в судьбах народов России: сб. материалов Всероссийской научно-практической конференции с международным участием / отв. ред. И. В. Фролова. Уфа, 2017. С. 105-110.', 31),
(774, 'Петров Ю. А. Россия накануне Великой революции 1917 г.: современные историографические тенденции // Российская история. 2017. № 2. С. 3-16.', 31),
(775, 'Проект государственной росписи доходов и расходов за 1917 г. Петроград: Тип. В. О. Киршебаума, 1916. 196 с.', 31),
(776, 'Рынков В. М. В преддверии военного коммунизма: государственная продовольственная политика как фактор трансформации хлебного рынка России (февраль-октябрь 1917 г.) // Известия Уральского федерального университета. Серия 2. Гуманитарные науки. 2017. Т. 19. № 3 (166). С. 105-119.', 31),
(777, 'Семенкова Т. Г. Министерство финансов Временного правительства в период изменения государственного строя России в 1917 году // Актуальные вопросы современной науки. 2013. № 29. С. 93-105.', 31),
(778, 'Сидоров А. Л. Экономическое положение России в годы Первой мировой войны. М.: Наука, 1973. 654 с.', 31),
(779, 'Соколов А. С. Государственное регулирование хлебных цен в Рязанской губернии (март-октябрь 1917 г.) // Экономическая история. 2018. Т. 14. № 1. С. 88-96.', 31),
(780, 'Статистический сборник за 1913-1917 гг. М.: ЦСУ, 1922. Вып. 2. 307 с.', 31);

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(5) NOT NULL,
  `tell` varchar(255) NOT NULL,
  `tell2` varchar(255) NOT NULL,
  `insta` varchar(255) NOT NULL,
  `metrika` text NOT NULL,
  `dopcod` text NOT NULL,
  `mup` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(5) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `auth_key`, `role`) VALUES
(1, 'admin', '123', 'RnHbjbvAkwtRFnVwFNN5vz7nRBldh4WB', 'admin');

-- --------------------------------------------------------

--
-- Структура таблицы `vid`
--

CREATE TABLE `vid` (
  `id` int(5) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `cvet` varchar(255) NOT NULL,
  `sostav` varchar(1000) NOT NULL,
  `activ` varchar(255) NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `codes`
--
ALTER TABLE `codes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `errors_log`
--
ALTER TABLE `errors_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `fields_values`
--
ALTER TABLE `fields_values`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `keywords`
--
ALTER TABLE `keywords`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `keywords_values`
--
ALTER TABLE `keywords_values`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `magazines`
--
ALTER TABLE `magazines`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `opercards`
--
ALTER TABLE `opercards`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `references_t`
--
ALTER TABLE `references_t`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `vid`
--
ALTER TABLE `vid`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT для таблицы `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1022548;

--
-- AUTO_INCREMENT для таблицы `codes`
--
ALTER TABLE `codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `errors_log`
--
ALTER TABLE `errors_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;

--
-- AUTO_INCREMENT для таблицы `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `fields_values`
--
ALTER TABLE `fields_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `keywords`
--
ALTER TABLE `keywords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT для таблицы `keywords_values`
--
ALTER TABLE `keywords_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT для таблицы `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `magazines`
--
ALTER TABLE `magazines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `opercards`
--
ALTER TABLE `opercards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `references_t`
--
ALTER TABLE `references_t`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=781;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `vid`
--
ALTER TABLE `vid`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
