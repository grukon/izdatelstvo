<?php

namespace app\modules\admin\controllers;
use app\modules\admin\models\Articles;
use app\modules\admin\models\Keywords;
use app\modules\admin\models\KeywordsValues;
use app\modules\admin\models\References;
use app\modules\admin\models\Codes;
use yii\web\Controller;
use yii\web\UploadedFile;
use Yii;

/**
 * Default controller for the `admin` module
 */
class ArticlesController extends AppAdminController
{


    public function actionIndex()
    {
        $posts = Articles::find()->all();

        return $this->render('index', compact('posts'));
    }
    
    public function actionEdit()
    {
        $id = Yii::$app->request->get('id');
        $post = Articles::find()->where(['id' => $id])->one();
        
        $edit = Articles::findOne($id);

        $other_attributs = self::getOtherAttributes($edit->id);

        if(!empty(Yii::$app->request->post())){

            $post = Yii::$app->request->post();
            foreach($post['Articles'] as $key => $value){
                $edit->$key = $value;
            }

            if($edit->save()){
                Yii::$app->session->setFlash('success','Редактирование прошло успешно.');
                return $this->refresh();
            }else{
                Yii::$app->session->setFlash('error','Ошибка редактирования!');
            }
         }

        return $this->render('edit', compact('edit','post','other_attributs'));
    }

    private function getOtherAttributes($article_id){
        $array = array();

        $references = References::find()->where(['article_id' => $article_id])->all();
        if (sizeof($references) > 0){
            $array['references'] = $references;
        }

        $codes = Codes::find()->where(['article_id' => $article_id])->all();
        if (sizeof($codes) > 0){
            $array['codes'] = $codes;
        }

        $keywords = Keywords::find()->where(['article_id' => $article_id])->all();
        if (sizeof($keywords) > 0){
            $array['keywords'] = array();
            foreach($keywords as $keyword){
                echo $keyword->value_id;
                $array['keywords'][] = KeywordsValues::find()->where(['id' => $keyword->value_id])->one();;

            }
        }
        return $array;
    }
    
    
    public function actionActiv()
    {
        
        $id = Yii::$app->request->get('id');
        $activ = Yii::$app->request->get('activ');
        
        $edit = Articles::findOne($id);
        $edit->activ = $activ;
        $edit->save();
        
    }
    
    
    
    public function actionNew()
    {
        $edit = new Articles;
        if( $edit->load(Yii::$app->request->post())){
   
            if($edit->save()){
                
                $edit->image = UploadedFile::getInstance($edit, 'image');
                
                if ($edit->image) {
                
                    $edit->upload();
                
                }
                
                Yii::$app->session->setFlash('success','Пост добавлен успешно.');
                return $this->refresh();
            }else{
                Yii::$app->session->setFlash('error','Ошибка добавления!');
            }
         }
        return $this->render('new', compact('edit'));
        
    }
    
    
    
        public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        $customer = Articles::findOne($id);
        
        return $this->render('delete');
    }
    
    
}
