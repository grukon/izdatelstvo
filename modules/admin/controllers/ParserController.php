<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\XmlParser;

class ParserController extends AppAdminController{
    
    public function actionIndex(){
        $this->layout = "empty";
        Yii::$app->controller->enableCsrfValidation = false;        
        $this->enableCsrfValidation = false;

        $model = new XmlParser();

        return $this->render('index', [
            "model" => $model
        ]);
    }

    

}