<?php

use mihaildev\ckeditor\CKEditor;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Статьи. Добавление';
?>


<h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Добавление статьи</h4>
<br>



<?php if(Yii::$app->session->hasFlash('success')): ?>
        
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <?php echo Yii::$app->session->getFlash('success'); ?>
        </div>
        
   <?php endif; ?>
   
   <?php if(Yii::$app->session->hasFlash('error')): ?>
        
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <?php echo Yii::$app->session->getFlash('error'); ?>
        </div>
        
   <?php endif; ?>


<div class="col-xs-12 col-sm-6 col-md-6">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']])?>
    
    <?php 
    
        $attributes = $edit->attributeLabels();
        foreach($edit->attributes as $key => $value){
            switch($key){
                case "id":
                    break;
                default:
                    if (in_array($key, $edit->attributeLabels())) 
                        echo $form->field($edit, $key)->textInput(['value' => $value])->label($attributes[$key]);
                    else 
                        echo $form->field($edit, $key)->textInput(['value' => $value]);
                    break;
            }
           
        }
    ?>
    <?= html::submitButton('Сохранить', ['class' => 'btn btn-success'])?>
    <?php ActiveForm::end()?>
    
    <div class="clearfix"></div>

    
    
</div>





