<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);

$this->beginPage();   
?>
<html>
<head>
    <?= Html::csrfMetaTags() ?>
</head>
<body>
<?php
echo $content;
?>
</body>
</html>
<?php
$this->endPage(); 
?>