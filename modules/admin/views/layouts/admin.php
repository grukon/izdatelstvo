<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Админка. <?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
<?php if( Yii::$app->user->identity['role'] != 'admin'){?>
<script type="text/javascript">
  location.replace("/");
</script>
<?php } ?>
</head>

<body style="background:none;">
 <?php $this->beginBody() ?>

<div id="admin" class="container-fluid">
    
    <div class="admin-bg-ff"> 
    
    <div id="admin-top">
    <div class="col-xs-12">
    
    <div class="but"><a href="/admin"><i class="fa fa-home" aria-hidden="true"></i></a></div>
    <!-- <div class="but"><a href="/" target="_blank"><i class="fa fa-globe" aria-hidden="true"></i></a></div> -->
    
    <!-- <div class="but" style="float:right;"><a href="/admin/settings"><i class="fa fa-cog" aria-hidden="true"></i></a></div> -->
    
    <p align="center" style="font-size:18px; padding-top:10px;"><i class="fa fa-clock-o" aria-hidden="true" style="color:#8a8a8a;"></i>  <span id="hours">00</span>:<span id="min">00</span>:<span id="sec">00</span>   <?php // echo (date("H:i:s")); ?></p>
    
    </div>    
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-2">
            <div class="">
            <div class="admin-left-menu">
                <a href="/admin"><div class="admin-ava"></div></a>
                
                <p align="center">Admin</p>
                
                
                <br>
                <a class="menu" href="/admin/magazines">Журналы</a>
                <a class="menu" href="/admin/authors">Авторы</a>
                <a class="menu" href="/admin/articles">Статьи</a>
                
                <br>
                <a class="menu" href="<?= \yii\helpers\Url::to(['/site/logout'])?>">
                    Выход
                </a>
                
            </div>    
            </div>
        </div>
    
        <div class="col-xs-12 col-sm-9 col-md-10 admin-content">
            
        <div class="col-xs-12">   
             <h4><i class="fa fa-cogs" aria-hidden="true"></i> <?= Html::encode($this->title) ?></h4>
             <hr>
             <div class="clearfix"></div>
             
            <?= $content ?>
            
           
         </div>    
        </div>

    </div>
    </div>
    
    <div class="container admin-footer">
        <p align="center">© <?=date("Y", time())?>. All rights reserved.</p>
    </div>
</div>

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic-ext" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/jquery.fileupload.css">

<?php $this->endBody() ?>
<script src="/js/jquery.ui.widget.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/js/jquery.fileupload.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="/js/admin.js"></script>

</body>
</html>
<?php $this->endPage() ?>