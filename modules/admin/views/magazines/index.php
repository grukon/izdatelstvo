<?php

/* @var $this yii\web\View */

$this->title = 'Журналы';
?>

<div class="row">
    <div class="col-xs-12 col-sm-6">
        <h4>Все журналы:</h4>
    </div>
    <div class="col-xs-12 col-sm-6" align="right">
        
        <a class="btn btn-success" href="/admin/magazines/new">
            <i class="fa fa-plus" aria-hidden="true"></i> Добавить журнал
        </a>
        
    </div>
</div>
<br>  


<div class="table-responsive">
          <table class="table table-hover">
            <tr>
            <th>№</th>
            <th>Название</th>
            <th>Часть</th>
            <th>Номер</th>
            <th>Страниц</th>
            <th></th>
            <th></th>
            </tr>
          
        
       <?php foreach($posts as $spisok):  ?>
        
            <tr>
                <td><?= $spisok->id ?></td>
                <td>
                    <?= $spisok->title ?>
                </td>
                <td><?= $spisok->volume ?></td>
                <td><?= $spisok->number ?></td>
                <td>
                    <?= $spisok->pages ?>
                </td>
                <td><a title="Редактирование" class="btn btn-success" href="/admin/magazines/edit/<?= $spisok->id ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                <td><a title="Удаление" class="btn btn-danger" href="/admin/magazines/delete/<?= $spisok->id ?>"><i class="fa fa-times" aria-hidden="true"></i></a></td>
            </tr>
            </tr>
        
        <?php endforeach ?>

        
    
    
    </table>
    
    </div>