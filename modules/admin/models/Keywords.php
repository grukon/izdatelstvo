<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;


class Keywords extends ActiveRecord{
    

    public static function tableName(){
        return 'keywords';
    }

   
   public function behaviors()
    {
        return [];
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'caption' => 'Название',
        ];
    }
    
}