<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;


class References extends ActiveRecord{
    

    public static function tableName(){
        return 'references_t';
    }

   
   public function behaviors()
    {
        return [];
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'caption' => 'Название',
        ];
    }
    
}