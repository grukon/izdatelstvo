<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;


class Codes extends ActiveRecord{
    

    public static function tableName(){
        return 'codes';
    }

   
   public function behaviors()
    {
        return [];
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'caption' => 'Название',
        ];
    }
    
}