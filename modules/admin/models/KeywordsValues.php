<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;


class KeywordsValues extends ActiveRecord{
    

    public static function tableName(){
        return 'keywords_values';
    }

   
   public function behaviors()
    {
        return [];
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'caption' => 'Название',
        ];
    }
    
}